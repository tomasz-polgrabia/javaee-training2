'use strict';

const gulp = require('gulp');
const webserver = require('gulp-webserver');
const webpack = require('webpack');
const path = require('path');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

let config = {
    entry: 'app',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
    },
    context: __dirname,
    target: 'web',
    mode: 'development',
    resolve: {
        modules: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'app')
        ],
        extensions: ['.js', '.jsx'],
    }
};

function buildScripts() {
    return new Promise(resolve => webpack(config, (err, stats) => {
        if (err) {
            console.log('Webpack', err);
        }

        console.log(stats.toString({}));
        resolve();
    }));
}

function buildHtml() {
    return gulp.src('app/**/*.html')
        .pipe(gulp.dest('dist'));
}

function buildScss() {
    return gulp.src('app/**/*.scss')
        .pipe(sass({
            includePaths: [
                "node_modules"
            ]
        }).on('error', sass.logError))
        .pipe(gulp.dest('dist'));
}

function serve() {
    return gulp.src('dist')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            open: false
        }));
}

exports.buildAll = gulp.parallel(buildScripts, buildHtml, buildScss);
exports.serve = serve;
exports.buildScripts = buildScripts;
exports.buildHtml = buildHtml;
exports.buildScss = buildScss;
exports.default = buildScripts;