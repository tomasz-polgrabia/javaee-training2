const axios = require('axios');

console.log(`hello world ${1+2}`);

axios.get('/index.js')
    .then(e => {
        console.log(`It works ${e.data}`);
    })
    .catch(e => {
        console.log(`It failed ${e}`);
    })