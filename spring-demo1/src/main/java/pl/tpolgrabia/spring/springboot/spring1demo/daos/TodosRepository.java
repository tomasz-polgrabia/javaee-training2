package pl.tpolgrabia.spring.springboot.spring1demo.daos;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.tpolgrabia.spring.springboot.spring1demo.entities.Todo;

public interface TodosRepository extends MongoRepository<Todo, String> {
}
