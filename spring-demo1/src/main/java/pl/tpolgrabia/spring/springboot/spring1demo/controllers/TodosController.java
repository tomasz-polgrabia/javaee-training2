package pl.tpolgrabia.spring.springboot.spring1demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.tpolgrabia.spring.springboot.spring1demo.daos.TodosRepository;

@Controller
@RequestMapping(path = "/todos")
public class TodosController {

    @Autowired
    private TodosRepository todosRepository;

    @RequestMapping(path = "/index",method = RequestMethod.GET)
    public String handleIndex(Model model) {
        model.addAttribute("todos", todosRepository.findAll());
        return "todos";
    }
}
