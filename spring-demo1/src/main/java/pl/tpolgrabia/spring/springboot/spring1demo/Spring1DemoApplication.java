package pl.tpolgrabia.spring.springboot.spring1demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring1DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Spring1DemoApplication.class, args);
    }
}
