const gulp = require('gulp');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const webpack = require('webpack');
const webserver = require('gulp-webserver');

let cssLoader = {
    loader: 'css-loader',
    options: {
        sourceMap: true
    }
};

let config = {
    devtool: 'source-map',
    entry: {
        index: [
            'index.js'
        ]
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/env']
                }
            },
            {
                test: /\.js$/,
                loader: 'source-map-loader',
                enforce: 'pre'
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    cssLoader
                ]
            },
            {
                test: /\.s[ac]ss$/,
                use: [
                    'vue-style-loader',
                    cssLoader,
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('node-sass'),
                            sourceMap: true,
                            sassOptions: {
                                fiber: require('fibers')
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'app')
        ],
        extensions: ['.js', '.vue', 'css', 'scss', 'sass']
    },
    target: 'web'
};

function buildJavascript() {
    return new Promise(resolve => {
        webpack(config, (error, status) => {
            if (error) {
                console.log(error.toString({}));
            }

            console.log(status.toString({}));
            resolve();
        })
    })
}

function buildHtml() {
    return gulp.src('app/**/*.html')
        .pipe(gulp.dest('dist'));
}

function serve() {
    return gulp.src('dist')
        .pipe(webserver({
            directoryListing: false,
            fallback: 'index.html',
            livereload: true,
            open: false,
            proxies: [
                {source: '/api', target: 'http://localhost:8080/'}
            ]
        }));
}

exports.buildHtml = buildHtml;
exports.buildJavascript = buildJavascript;
exports.build = gulp.parallel(buildJavascript, buildHtml);
exports.serve = serve;
exports.default = exports.build;
