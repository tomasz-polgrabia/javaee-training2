import Aboutme from "./Aboutme";
import News from 'News';
import Login from 'Login';
import Vue from 'vue/dist/vue.js';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import 'index.scss';
import Logout from "./Logout";

Vue.use(VueRouter);
Vue.use(Vuex);

const routes = [
    {
        path: '/', component: News
    },
    {
        path: '/aboutme', component: Aboutme
    },
    {
        path: '/login', component: Login
    },
    {
        path: '/logout', component: Logout
    }
]

const router = new VueRouter({
   routes
});

const store = new Vuex.Store({
    state: {
        loggedin: false,
        username: ""
    },
    mutations: {
        login(state, payload) {
            console.log('Mutation login');
            state.loggedin = true;
            state.username = payload.username;
            document.querySelector("#logout-nav-node").style.display = 'block';
            document.querySelector("#login-username-nav-node").style.display = 'block';
            document.querySelector("#login-username-nav-node").innerHTML = `Welcome ${state.username}`;
            document.querySelector("#login-nav-node").style.display = 'none';
        },
        logout(state) {
            console.log('Mutation logout');
            state.loggedin = false;
            state.username = "";
            document.querySelector("#logout-nav-node").style.display = 'none';
            document.querySelector("#login-username-nav-node").style.display = 'none';
            document.querySelector("#login-nav-node").style.display = 'block';
        }
    }
});

const app = new Vue({
    router: router,
    store: store
}).$mount('#app');