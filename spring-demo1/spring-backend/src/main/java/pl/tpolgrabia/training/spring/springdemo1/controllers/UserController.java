package pl.tpolgrabia.training.spring.springdemo1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.tpolgrabia.training.spring.springdemo1.entities.User;
import pl.tpolgrabia.training.spring.springdemo1.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(it -> {
            users.add(it);
        });
        return ResponseEntity.ok(users);
    }
}
