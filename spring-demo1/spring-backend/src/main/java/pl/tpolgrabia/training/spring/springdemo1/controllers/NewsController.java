package pl.tpolgrabia.training.spring.springdemo1.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.tpolgrabia.training.spring.springdemo1.dtos.NewsDto;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/news")
public class NewsController {

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ResponseEntity<List<NewsDto>> handleIndex() {
        NewsDto news1 = new NewsDto();
        NewsDto news2 = new NewsDto();
        news1.setId(1L);
        news1.setSubject("News 1");
        news1.setContent("Content 1");
        news2.setId(2L);
        news2.setSubject("News 2");
        news2.setContent("Content 2");
        List<NewsDto> data = Arrays.asList(news1, news2);
        return ResponseEntity.ok(data);
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> handlePost() {
        Map<String, String> data = new HashMap<>();

        data.put("Status", "OK");
        return ResponseEntity.ok(data);
    }

}
