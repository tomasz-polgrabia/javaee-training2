package pl.tpolgrabia.training.spring.springdemo1.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.tpolgrabia.training.spring.springdemo1.entities.Authority;
import pl.tpolgrabia.training.spring.springdemo1.entities.AuthorityId;
import pl.tpolgrabia.training.spring.springdemo1.entities.User;
import pl.tpolgrabia.training.spring.springdemo1.repositories.AuthoritiesRepository;
import pl.tpolgrabia.training.spring.springdemo1.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class JpaAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthoritiesRepository authoritiesRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String providedUserName = authentication.getName();
        String providedPassword = authentication.getCredentials().toString();
        Optional<User> ouser = userRepository.findById(providedUserName);
        if (ouser.isPresent()) {
            User user = ouser.get();
            List<Authority> authorityCodes = authoritiesRepository.findByUsername(user.getUsername());
            List<SimpleGrantedAuthority> authorities = authorityCodes.stream()
                    .map(e -> new SimpleGrantedAuthority(e.getAuthority()))
                    .collect(Collectors.toList());
            boolean credentialsMatch = passwordEncoder.matches(providedPassword, user.getPassword());
            return credentialsMatch ?
                    new UsernamePasswordAuthenticationToken(providedUserName,
                            ouser.get().getPassword(),
                            authorities)
                    : null;
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
