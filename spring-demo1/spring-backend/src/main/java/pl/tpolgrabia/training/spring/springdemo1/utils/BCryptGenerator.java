package pl.tpolgrabia.training.spring.springdemo1.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptGenerator {
    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        for (int i = 0; i < args.length; i++) {
            System.out.println(String.format(
                    "%s encrypted to %s",
                    args[i],
                    encoder.encode(args[i])
            ));
        }
    }
}
