package pl.tpolgrabia.training.spring.springdemo1.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.tpolgrabia.training.spring.springdemo1.entities.User;

public interface UserRepository extends CrudRepository<User, String> {
    
}
