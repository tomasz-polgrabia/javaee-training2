package pl.tpolgrabia.training.spring.springdemo1.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.tpolgrabia.training.spring.springdemo1.entities.Authority;
import pl.tpolgrabia.training.spring.springdemo1.entities.AuthorityId;

import java.util.List;

public interface AuthoritiesRepository extends CrudRepository<Authority, AuthorityId> {
    List<Authority> findByUsername(String username);
}
