const gulp = require('gulp');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const webpack = require('webpack');
const webserver = require('gulp-webserver');

let config = {
    devtool: 'source-map',
    entry: {
        index: [
            'index.js'
        ]
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                include: [path.resolve(__dirname, 'app')],
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/env']
                }
            },
            {
                test: /\.js$/,
                include: [path.resolve(__dirname, 'app')],
                loader: 'source-map-loader',
                enforce: 'pre'
            },
            {
                test: /\.vue$/,
                include: [path.resolve(__dirname, 'app')],
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                include: [path.resolve(__dirname, 'app')],
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'app')
        ],
        extensions: ['.js', '.vue']
    },
    target: 'web'
};

function buildJavascript() {
    return new Promise(resolve => {
        webpack(config, (error, status) => {
            if (error) {
                console.log(error.toString({}));
            }

            console.log(status.toString({}));
            resolve();
        })
    })
}

function buildHtml() {
    return gulp.src('app/**/*.html')
        .pipe(gulp.dest('dist'));
}

function serve() {
    return gulp.src('dist')
        .pipe(webserver({
            directoryListing: false,
            fallback: 'index.html',
            livereload: true,
            open: false,
            proxies: [
                {source: '/api', target: 'http://localhost:3000/api'}
            ]
        }));
}

exports.buildHtml = buildHtml;
exports.buildJavascript = buildJavascript;
exports.build = gulp.parallel(buildJavascript, buildHtml);
exports.serve = serve;
exports.default = exports.build;
