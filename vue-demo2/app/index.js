import App from './App';
import Vue from 'vue';
import UserService from "./UserService";

Vue.mixin(UserService);

new Vue({
    el: '#app',
    render: h => h(App)
});
