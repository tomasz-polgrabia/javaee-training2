const gulp = require('gulp');
const path = require('path');
const webpack = require('webpack');
const webserver = require('gulp-webserver');

let config = {
    entry: {
        index: [
            './app/index.js'
        ]
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: [path.resolve(__dirname, "app")],
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/env', '@babel/react']
                }
            }
        ]
    },
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'app')
        ],
        extensions: ['.js', '.jsx']
    },
    target: 'web'
};

function buildJavascript() {
    return new Promise(resolve => {
        webpack(config, (error, status) => {

            if (error) {
                console.log(error.toString({}));
            }

            console.log(status.toString({}));
            resolve();
        });
    });
}

function serve() {
    return gulp.src('dist')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            fallback: 'index.html',
            open: false,
            proxies: [
                {
                    source: '/api',
                    target: 'http://localhost:3000/api'
                }
            ]
        }));
}

function buildHtml() {
    return gulp.src('app/**/*.html')
        .pipe(gulp.dest('dist'));
}

exports.serve = serve;
exports.buildJavascript = buildJavascript;
exports.buildHtml = buildHtml;
exports.build = gulp.parallel(buildJavascript, buildHtml);