package pl.tpolgrabia.trainings.todosdemo1.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.tpolgrabia.trainings.todosdemo1.entities.Todo;
import pl.tpolgrabia.trainings.todosdemo1.repositories.TodosRepository;

import java.util.List;

@RestController
@RequestMapping(path = "/todos")
public class TodosController {

    private static final Logger logger = LoggerFactory.getLogger(TodosController.class);

    @Autowired
    private TodosRepository todosRepository;

    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public ResponseEntity<List<Todo>> handleGetTodos(
            @RequestParam(name = "page", defaultValue = "0", required = false)
                    int page,
            @RequestParam(name = "size", defaultValue = "10", required = false)
                    int size) {
        logger.info("Page: {}, size: {}", page, size);
        Page<Todo> pageTodos = todosRepository.findAll(
                PageRequest.of(
                        page,
                        size,
                        Sort.Direction.ASC,
                        "id"));
        List<Todo> todos = pageTodos.getContent();
        return ResponseEntity.ok()
                .body(todos);
    }

    @RequestMapping(path = "/index", method = RequestMethod.POST)
    public ResponseEntity<List<Todo>> handlePostTodos(@RequestBody List<Todo> todos) {
        todosRepository.saveAll(todos);
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(todos);
    }

    @RequestMapping(path = "/index", method = RequestMethod.PUT)
    public ResponseEntity<List<Todo>> handlePutTodos(@RequestBody List<Todo> todos) {
        todosRepository.insert(todos);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(todos);
    }

    @RequestMapping(path = "/index", method = RequestMethod.DELETE)
    public ResponseEntity<List<Todo>> handleDeleteTodos(@RequestBody List<Todo> todos) {
        todosRepository.deleteAll(todos);
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(todos);
    }
}
