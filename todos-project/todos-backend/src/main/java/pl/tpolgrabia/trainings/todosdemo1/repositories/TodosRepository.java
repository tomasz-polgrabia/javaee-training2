package pl.tpolgrabia.trainings.todosdemo1.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.tpolgrabia.trainings.todosdemo1.entities.Todo;

public interface TodosRepository extends MongoRepository<Todo, String> {
}
