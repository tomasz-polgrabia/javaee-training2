package pl.tpolgrabia.trainings.todosdemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodosDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(TodosDemo1Application.class, args);
    }

}
