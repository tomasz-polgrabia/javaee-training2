import App from 'App';
import Vue from 'vue';
import 'bootstrap';
import 'bootstrap/scss/bootstrap.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';
// checked and both ways - scss & css work

new Vue({
    render: h => h(App)
}).$mount('#app');
