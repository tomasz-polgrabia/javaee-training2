public class BST {

    private static interface Visitor<S> {
        void accept(S node);
    }

    private static class TreeNode<T extends Comparable> {
        public TreeNode left;
        public TreeNode right;
        public T val;

        public TreeNode(T val) {
            this.val = val;
        }

        public TreeNode<T> search(T val) {
            if (this.val.compareTo(val) == 0) {
                return this;
            } else if (this.val.compareTo(val) < 0) {
                // being searched node is lower than the current root -> left subtree
                return left == null ? null : left.search(val);
            } else {
                // the right subtree
                return right == null ? null : right.search(val);
            }
        }

        public TreeNode<T> add(T node) {
            return add(new TreeNode(node));
        }

        public TreeNode<T> add(TreeNode<T> node) {

            if (node == null || node.val == null) {
                return this;
            }

            if (val.compareTo(node.val) == 0) {
                // already in the tree
                throw new IllegalStateException("Node " + node.val + " is already in the tree - " + val);
            } else if (val.compareTo(node.val) < 0 ) {
                // root is lower than being added node
                right = right == null ? node : right.add(node);
                return this;
            } else {
                // root is bigger than being added node
                left = left == null ? node : left.add(node);
                return this;
            }
        }

        public TreeNode<T> remove(T node) {
            return remove(new TreeNode<>(node));
        }

        public TreeNode<T> remove(TreeNode<T> node) {
            if (node == null) {
                return this;
            }

            if (node.val == val) {
                // we found it
                
                if (left == null) {
                    return right;
                }

                if (right == null) {
                    return left;
                }

                // new root is right
                // just adding previous left subtree to the preivous right's one (now the root one)

                right.left = right.left.add(node);
                return right;
            } else {
                // searching in subtrees
                if (node.val.compareTo(val) < 0) {
                    if (left != null) {                       
                        left = left.remove(node);
                    }
                    return this;
                } else {
                    if (right != null) {
                        right = right.remove(node);
                    }
                    return this;
                }
            }
        }

        public void infix(Visitor<TreeNode<T>> visitor) {
            if (left != null) {
                left.infix(visitor);
            }

            visitor.accept(this);

            if (right != null) {
                right.infix(visitor);
            }
        }

        public void prefix(Visitor<TreeNode<T>> visitor) {
            visitor.accept(this);

            if (left != null) {
                left.prefix(visitor);
            }

            if (right != null) {
                right.prefix(visitor);
            }
        }

        public void suffix(Visitor<TreeNode<T>> visitor) {

            if (left != null) {
                left.suffix(visitor);
            }

            if (right != null) {
                right.suffix(visitor);
            }

            visitor.accept(this);
        }

    }

    public static void main(String[] args) {
        var tree = new TreeNode<>(1);
        tree.add(2);
        tree.add(-1);
        System.out.println("Hello World!!!");
        tree.infix(e -> System.out.println("Node val " + e.val));
        System.out.println("");
        tree.prefix(e -> System.out.println("Node val " + e.val));
        System.out.println("");
        tree.suffix(e -> System.out.println("Node val " + e.val));


        System.out.println("");
        tree = tree.remove(2);
        tree.infix(e -> System.out.println("Node val " + e.val));
    
        System.out.println("");
        tree = tree.remove(10);
        tree.infix(e -> System.out.println("Node val " + e.val));

        if (tree.search(1) != null) {
            System.out.println("1 found");
        } else {
            System.out.println("1 not found");
        }

        if (tree.search(10) != null) {
            System.out.println("10 found");
        } else {
            System.out.println("10 not found");
        }
    }
}
