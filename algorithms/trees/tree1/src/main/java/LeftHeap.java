public class LeftHeap {

    private static class LeftHeapNode<T extends Comparable> {
        public LeftHeapNode<T> left = null;
        public LeftHeapNode<T> right = null;
        public int sdistance;
        public T val;

        @Override
        public String toString() {
            return "Node(" + val + ")";
        }

        public LeftHeapNode(T val) {
            this.val = val;
            this.sdistance = 1;
        }

        public LeftHeapNode<T> merge(LeftHeapNode<T> b) {
            var a = this;

            if (b == null) {
                // System.out.println("Case 1");
                return a;
            }

            // a never is null

            if (b.val.compareTo(a.val) > 0) {
                var temp = b;
                b = a;
                a = temp;
                // System.out.println("b.val > a.val");
            }

            // a.right = a.right == null ? b : a.right.merge(b);

            if (a.right == null) {
                // System.out.println("Case 2.1");
                a.right = b;
            } else {
                // System.out.println("Case 2.2");
                a.right = a.right.merge(b);
            }

            // a.right is non null as b is non null

            if (a.left == null) {
                a.left = a.right;
                a.right = null;
                a.sdistance = 1;

                // System.out.println("Case 3");
                return a;
            }

            if (a.right.sdistance > a.left.sdistance) {
                var temp = a.left;
                a.left = a.right;
                a.right = temp;
                // System.out.println("Case 4");
            }

            a.sdistance = a.right.sdistance + 1;

            return a;
        }

        public LeftHeapNode<T> add(T nval) {
            return merge(new LeftHeapNode<>(nval));
        }

        public LeftHeapNode<T> pop() {
            return right != null ? right.merge(left) : left;
        }

        public void dump() {
            dump("");    
        }

        private void dump(String path) {
            System.out.println(
                    String.format("Path %s, val: %s, s: %d (%s,%s)",
                        path,
                        val,
                        sdistance,
                        left,
                        right));

            if (left != null) {
                left.dump(path + "L");
            }

            if (right != null) {
                right.dump(path + "R");
            }
        }
    }

    public static void main(String[] args) {
        var heap = new LeftHeapNode<>(1);
        heap = heap.add(2);
        heap.add(-2);
        heap.add(3);
        heap.add(-20);

        heap.dump();

        var deleted = heap;
        heap = heap.pop();

        System.out.println(String.format("Val: %d", deleted.val));

        deleted = heap;
        heap = heap.pop();
        System.out.println(String.format("Val: %d", deleted.val));

        deleted = heap;
        heap = heap.pop();
        System.out.println(String.format("Val: %d", deleted.val));

        deleted = heap;
        heap = heap.pop();
        System.out.println(String.format("Val: %d", deleted.val));
        
    }
}
