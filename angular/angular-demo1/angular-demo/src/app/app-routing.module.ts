import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from "./app.component";
import {HelloWorldComponent} from "./hello-world/hello-world.component";
import {NotFoundComponent} from "./not-found/not-found.component";


const routes: Routes = [
  {path: '', component: HelloWorldComponent},
  {path: 'hello/:name', component: HelloWorldComponent},
  {path: '**', component: NotFoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
