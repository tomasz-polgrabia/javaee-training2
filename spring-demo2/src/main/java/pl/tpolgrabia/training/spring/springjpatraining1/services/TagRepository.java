package pl.tpolgrabia.training.spring.springjpatraining1.services;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.tpolgrabia.training.spring.springjpatraining1.dto.Tag;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class TagRepository {

    @PersistenceContext
    private EntityManager em;

    public void save(Tag tag) {
        em.persist(tag);
    }
}
