package pl.tpolgrabia.training.spring.springjpatraining1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.tpolgrabia.training.spring.springjpatraining1.dto.Post;
import pl.tpolgrabia.training.spring.springjpatraining1.dto.Tag;
import pl.tpolgrabia.training.spring.springjpatraining1.dto.User;
import pl.tpolgrabia.training.spring.springjpatraining1.services.PostRepository;
import pl.tpolgrabia.training.spring.springjpatraining1.services.TagRepository;
import pl.tpolgrabia.training.spring.springjpatraining1.services.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.UUID;

@SpringBootApplication
@EnableTransactionManagement
public class SpringJpaTraining1Application {
    private static final Logger logger = LoggerFactory.getLogger(SpringJpaTraining1Application.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private PostRepository postRepository;

    @PostConstruct
    public void init() {
        logger.info("Hello World");
        User u = new User();
        String id = UUID.randomUUID().toString();
        u.setFirstName("User first name " + id);
        u.setLastName("User last name " + id);

        userRepository.save(u);

        Tag t1 = new Tag();
        t1.setName("t " + UUID.randomUUID().toString());

        Post p1 = new Post();
        p1.setTags(Arrays.asList(t1));

        tagRepository.save(t1);
        postRepository.save(p1);

    }

    public static void main(String[] args) {
        SpringApplication.run(SpringJpaTraining1Application.class, args);
    }

}
