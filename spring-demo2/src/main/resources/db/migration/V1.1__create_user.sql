create table users
(
    id            serial,
    user_name     text,
    password      text,
    first_name    text,
    last_name     text,
    creation_date date,
    email         text unique
);