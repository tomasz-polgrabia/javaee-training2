create table posts
(
    id      serial primary key,
    subject text,
    content text,
    created date,
    updated date
);

create table tags
(
    id   serial primary key ,
    name text unique
);

create table posts_tags
(
    post_id bigint references posts (id),
    tag_id  bigint references tags (id),
    constraint posts_tags_key primary key (post_id, tag_id)
);