update users u
set locked = false
where locked is null;

alter table users
    alter column locked set not null,
    alter column locked set default false;