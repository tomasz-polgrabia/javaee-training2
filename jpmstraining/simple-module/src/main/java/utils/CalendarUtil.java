package utils;

import java.time.Clock;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CalendarUtil {
    public static String currentDateInYYYYMMdd() {
        return DateTimeFormatter.ISO_DATE.format(LocalDate.now(Clock.systemUTC()));
    }
}
